<?php session_start(); ?>
<?php require_once("/../config.php");   ?>

<!DOCTYPE html>
<html>
	<head>
		<title>Ajaxing</title>
		<meta charset="utf-8">
		<!-- <link rel='stylesheet' type='text/css' href='style_sheet_name.css'> --> 
		<script  src='/troubleshooter/jquery-1.9.1.js'></script>  
		<script  src='/troubleshooter/ajax_functions.js'></script>
		<script  src='/troubleshooter/config.js'></script>
		
	</head>
	<body>
		 <p>this should be ajaxing.... </p>
		 <div id="ajax_result"></div>
		<script>
		function callback(data){
			console.log(data);
			$("#ajax_result").html(data);
		}
			$(document).ready(function() {
 				var dataString = "foo=bar";
				var URL =  JS_CONFIG.BASE_URL+ "/ajax_server.php";
				ajax_datastring_URL_callback(dataString, URL, callback);
			});
		</script>

	</body>
</html>